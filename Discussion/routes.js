const http = require('http');

const port = 4000;

const server = http.createServer(function (request, response) {

	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Hello World!');
	} else if(request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the Homepage!');
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page not available.');
	}

})

server.listen(port);

console.log(`Server is now accessible at http://localhost:${port}`);